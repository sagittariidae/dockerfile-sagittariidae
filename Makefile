image-repo    := registry.gitlab.com/sagittariidae/dockerfile-sagittariidae
image-version := 0.0.3

images  = base cron service

.PHONY: all clean

all: $(foreach image,$(images),$(image)-image) service/sp-metadata.xml

%-image: service/sp-key.pem service/domain.key
	docker build -t $(image-repo)/$*:$(image-version) $*

service/sp-metadata.xml: service-image
	docker run $(image-repo)/service:$(image-version) shib-metagen -c /etc/shibboleth/sp-cert.pem -h hylos.mines.edu -e https://hylos.mines.edu/hylos/sp > $@

service/sp-key.pem: service/sp-key.pem.gpg
	gpg --decrypt $< > $@

service/domain.key: service/domain.key.gpg
	gpg --decrypt $< > $@

clean:
	for image in $(images); do \
		docker rmi -f $(image-repo-prefix)-$${image}:$(image-version); \
	done
